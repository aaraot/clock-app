class AnalogClock extends Clock {
    constructor(slot, location) {
        super(slot, location);
    }

    build() {
        const TEMPLATE = '<div id="analog-clock" class="analog-clock--container animated fadeIn">' +
            '<div class="clock analog-clock">' +
            '<div class="hours--container">' +
            '<div class="hours"></div>' +
            '</div>' +
            '<div class="minutes--container">' +
            '<div class="minutes"></div>' +
            '</div>' +
            '<div class="seconds--container">' +
            '<div class="seconds"></div>' +
            '</div>' +
            '</div>' +
            '</div>';

        return TEMPLATE;
    }

    init() {
        const ANALOGCLOCK = this.slot.find('#analog-clock'),
            ELEMENTS = ANALOGCLOCK.find('.hours, .minutes, .seconds'),
            DATE = moment.tz(this.location),
            HOURS = DATE.format('h'),
            MINUTES = DATE.format('mm'),
            SECONDS = DATE.format('ss'),
            // Create an object with each pointer and it's angle in degrees
            pointers = [
                {
                    pointer: 'hours',
                    angle: (HOURS * 30) + (MINUTES / 2)
                },
                {
                    pointer: 'minutes',
                    angle: (MINUTES * 6)
                },
                {
                    pointer: 'seconds',
                    angle: (SECONDS * 6)
                }
            ];

        // Loop through each of these pointers to set their angle
        for (let x = 0; x < ELEMENTS.length; x++) {
            ELEMENTS[x].style.transform = 'rotateZ(' + pointers[x].angle + 'deg)';

            // If this is a minute pointer, note the seconds position (to calculate minute position later)
            if (pointers[x].pointer === 'minutes') {
                ELEMENTS[x].parentNode.setAttribute('data-second-angle', pointers[x + 1].angle);
            }
        }

        this.setUpMinutePointers(ANALOGCLOCK);
        this.moveSecondPointers(ANALOGCLOCK);
    }

    // Set a timeout for the first minute pointer movement (less than 1 minute), then rotate it every minute after that
    setUpMinutePointers(container) {
        // Find out how far into the minute we are
        const CONTAINER = container.find('.minutes--container'),
            SECONDANGLE = CONTAINER.attr('data-second-angle');

        if (SECONDANGLE > 0) {
            // Set a timeout until the end of the current minute, to move the pointer
            const DELAY = (((360 - SECONDANGLE) / 6) + 0.1) * 1000;

            setTimeout(this.moveMinutePointers(CONTAINER), DELAY, this);
        }
    }

    // Do the first minute's rotation
    moveMinutePointers(container) {
        container.css('transform', 'rotateZ(6deg)');

        // Then continue with a 60 second interval
        setInterval(function () {
            if (container.angle === undefined) {
                container.angle = 12;
            } else {
                container.angle += 6;
            }

            container.css('transform', 'rotateZ(' + container.angle + 'deg)');
        }, 60000);
    }

    // Move the second containers
    moveSecondPointers(container) {
        const CONTAINER = container.find('.seconds--container');

        setInterval(function () {
            if (CONTAINER.angle === undefined) {
                CONTAINER.angle = 6;
            } else {
                CONTAINER.angle += 6;
            }

            CONTAINER.css('transform', 'rotateZ(' + CONTAINER.angle + 'deg)');
        }, 1000);
    }
}