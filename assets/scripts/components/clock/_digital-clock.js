class DigitalClock extends Clock {
    constructor(slot, location) {
        super(slot, location);

        this.interval;
    }

    build() {
        const TEMPLATE = '<div id="digital-clock" class="digital-clock--container animated fadeIn">' +
            '<div class="clock digital-clock">' +
            '<div class="numbers">' +
            '<p class="hours"></p>' +
            '</div>' +
            '<div class="colon">' +
            '<p>:</p>' +
            '</div>' +
            '<div class="numbers">' +
            '<p class="minutes"></p>' +
            '</div>' +
            '<div class="colon">' +
            '<p>:</p>' +
            '</div>' +
            '<div class="numbers">' +
            '<p class="seconds"></p>' +
            '</div>' +
            '<div class="am-pm">' +
            '<div>' +
            '<p class="am">am</p>' +
            '</div>' +
            '<div>' +
            '<p class="pm">pm</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        return TEMPLATE;
    }

    init() {
        this.interval = setInterval(function () {
            const DIGITALCLOCK = this.slot.find('#digital-clock'),
                ELEMENTS = DIGITALCLOCK.find('.hours, .minutes, .seconds, .am, .pm'),
                DATE = moment.tz(this.location);
            let hours = DATE.format('h'),
                minutes = DATE.format('m'),
                seconds = DATE.format('s');

            // Make clock a 12 hour clock instead of 24 hour clock
            hours = (hours > 12) ? (hours - 12) : hours;
            hours = (hours === 0) ? 12 : hours;

            // Invokes function to make sure number has at least two digits
            hours = this.addZero(hours);
            minutes = this.addZero(minutes);
            seconds = this.addZero(seconds);

            // Changes the html to match results
            ELEMENTS[0].innerHTML = hours;
            ELEMENTS[1].innerHTML = minutes;
            ELEMENTS[2].innerHTML = seconds;

            this.ampm(ELEMENTS);
        }.bind(this), 1000);
    }

    // Turns single digit numbers to two digit numbers by placing a zero in front
    addZero(val) {
        return (val <= 9) ? ('0' + val) : val;
    }

    // Lights up either am or pm on clock
    ampm(elements) {
        const DATE = moment.tz(this.location),
            HOURS = DATE.format('h'),
            AM = elements[3].classList,
            PM = elements[4].classList;

        (HOURS >= 12) ? PM.add('light-on') : AM.add('light-on');
        (HOURS >= 12) ? AM.remove('light-on') : PM.remove('light-on');
    }

    destroy() {
        clearInterval(this.interval);
    }
}