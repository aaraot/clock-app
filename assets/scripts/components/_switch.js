class Switch {
    constructor() {
    }

    build() {
        const TEMPLATE = '<div class="checkbox">' +
            '<p>' +
            '<label>' +
            '<input type="checkbox" />' +
            '<span>Digital</span>' +
            '</label>' +
            '</p>' +
            '</div>';

        return TEMPLATE;
    }
}