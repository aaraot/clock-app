// Gulp.js configuration
// modules
const gulp = require('gulp'),
    // Scripts
    deporder = require('gulp-deporder'),
    order = require("gulp-order"),
    concat = require('gulp-concat'),
    babel = require('gulp-babel'),
    uglify = require('gulp-uglify'),
    stripdebug = require('gulp-strip-debug'),
    clean = require('gulp-clean'),
    // Styles
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    csso = require('gulp-csso'),
    rename = require("gulp-rename"),
    // Watch
    watch = require('gulp-watch'),
    // Assets folders
    folder = {
        src: './assets/',
        build: './build/'
    };

// Set the browser that you want to support
const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

// JavaScript pages optimization
gulp.task('scripts:components', function () {
    return gulp.src(folder.src + 'scripts/components/**/*')
        .pipe(order([
            'clock/_clock.js',
            'clock/_analog-clock.js',
            'clock/_digital-clock.js'
        ]))
        .pipe(concat('components.min.js'))
        // .pipe(stripdebug())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify().on('error', function (e) {
            console.log(e);
        }))
        .pipe(gulp.dest(folder.build + 'scripts/'));
});

gulp.task('scripts:pages', function () {
    return gulp.src(folder.src + 'scripts/pages/**/*')
        .pipe(deporder())
        .pipe(concat('pages.min.js'))
        // .pipe(stripdebug())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify().on('error', function (e) {
            console.log(e);
        }))
        .pipe(gulp.dest(folder.build + 'scripts/'));
});

gulp.task('scripts:join', function () {
    return gulp.src([folder.build + 'scripts/components.min.js', folder.build + 'scripts/pages.min.js'])
        .pipe(deporder())
        .pipe(concat('main.min.js'))
        .pipe(uglify().on('error', function (e) {
            console.log(e);
        }))
        .pipe(clean())
        .pipe(gulp.dest(folder.build + 'scripts/'));
});

// Sass components optimization
gulp.task('styles', function () {
    return gulp.src(folder.src + 'styles/theme.scss')
        .pipe(sass())
        .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
        .pipe(csso())
        .pipe(rename(function (path) {
            path.dirname = "styles/";
            path.basename += ".min";
            path.extname = ".css";
        }))
        .pipe(gulp.dest(folder.build))
});

// Watch all tasks
gulp.task('watch:all', function () {
    // Watch for scripts changes
    gulp.watch(folder.src + 'scripts/**/*', gulp.series('scripts:components', 'scripts:pages', 'scripts:join'));

    // Watch for styles changes
    gulp.watch(folder.src + 'styles/theme.css', gulp.series('styles'));
});

// Run tasks
gulp.task('run', gulp.series('scripts:components', 'scripts:pages', 'scripts:join', 'styles'));

// Watch tasks
gulp.task('watch', gulp.series('watch:all'));