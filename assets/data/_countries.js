class Countries {
    constructor() {
        this.countryNames = new Object();
        for (let country of moment.tz.names()) {
            this.countryNames[country] = null;
        }
    }

    getCountries() {
        return this.countryNames;
    }
}