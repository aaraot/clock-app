class Slot {
    constructor(id, location) {
        this.id = id;
        this.location = location;
    }

    build() {
        const TEMPLATE = `<section id="slot-${this.id}" class="slot center-align">` +
            '<i class="material-icons">close</i>' +
            `<h6>${this.location}</h6>` +
            '<div class="clocks"></div>' +
            '</section>';

        return TEMPLATE;
    }

    init() {
        const SLOT = $('#slot-' + this.id),
            ANALOGCLOCK = new AnalogClock(SLOT, this.location),
            INFO = new Info(SLOT, this.location);

        SLOT.find('.clocks').append(ANALOGCLOCK.build());
        SLOT.append(INFO.build());
        SLOT.append(new Switch().build());

        ANALOGCLOCK.init();
        INFO.init();

        $(document).on('click', `.clocks--container #slot-${this.id} .material-icons`, function () {
            $(this).parent().remove();
        });

        $(document).on('click', `.clocks--container #slot-${this.id} input[type=checkbox]`, function () {
            const HASANALOGCLOCK = SLOT.find('#analog-clock');
            let digitalClock = new DigitalClock(SLOT, this.location);

            if (HASANALOGCLOCK.length > 0) {
                HASANALOGCLOCK.remove();
                SLOT.find('.clocks').append(digitalClock.build());

                digitalClock.init();
            } else {
                const ANALOGCLOCK = new AnalogClock(SLOT, this.location);

                digitalClock.destroy();
                SLOT.find('#digital-clock').remove();
                SLOT.find('.clocks').append(ANALOGCLOCK.build());

                ANALOGCLOCK.init();
            }
        });
    }
}