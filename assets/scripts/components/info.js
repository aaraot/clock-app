class Info {
    constructor(slot, location) {
        this.slot = slot;
        this.location = location;
    }

    build() {
        const TEMPLATE = '<div class="info--container">' +
            '<span class="clock-date"></span>' +
            '<span class="clock-timezone"></span>' +
            '</div>';

        return TEMPLATE;
    }

    init() {
        const INFO = this.slot.find('.info--container'),
            DATE = moment.tz(this.location);

        INFO.find('.clock-date').html(DATE.format('MMM d, Y'));
        INFO.find('.clock-timezone').html(DATE.format('Z z'));
    }
}