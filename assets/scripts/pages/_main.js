(function () {
    'use strict'

    new Search();

    $('.add-clock').on('click', function () {
        const SLOT = new Slot($('.slot').length, $('#autocomplete-input').val());

        $('.clocks--container').append(SLOT.build());

        SLOT.init();

        reset();
    });

    // Reset
    function reset() {
        $('#autocomplete-input').val('');
    }
})();